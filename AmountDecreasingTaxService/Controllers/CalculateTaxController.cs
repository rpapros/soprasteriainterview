﻿using AmountDecreasingTaxService.Commands;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AmountDecreasingTaxService.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CalculateTaxController : ControllerBase
    {
        private readonly ILogger<CalculateTaxController> _logger;

        public CalculateTaxController(ILogger<CalculateTaxController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public decimal? Get(decimal amount)
        {
            CalculateTax calculateTax = new CalculateTax2019()
            {
                Amount = amount,
                DecreasingTaxCommand = new CalculateAmountDecreasingTax2019
                {
                    Amount = amount
                }
            };

            calculateTax.Invoke();

            return calculateTax.Result;
        }
    }
}