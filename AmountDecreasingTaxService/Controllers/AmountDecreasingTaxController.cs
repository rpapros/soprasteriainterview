﻿using AmountDecreasingTaxService.Commands;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AmountDecreasingTaxService.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AmountDecreasingTaxController : ControllerBase
    {
        private readonly ILogger<AmountDecreasingTaxController> _logger;

        public AmountDecreasingTaxController(ILogger<AmountDecreasingTaxController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public decimal? Get(decimal amount)
        {
            CalculateAmountDecreasingTax calcDecreasingTax = new CalculateAmountDecreasingTax2019()
            {
                Amount = amount
            };

            calcDecreasingTax.Invoke();
            
            return calcDecreasingTax.Result;
        }
    }
}
