﻿namespace AmountDecreasingTaxService.Commands
{
    public class CalculateAmountDecreasingTax2019 : CalculateAmountDecreasingTax
    {
        protected override decimal Calculate()
        {
            decimal calculatedAmount = 0m;

            switch (Threshold)
            {
                case ThresholdEnum.Threshold1:
                    calculatedAmount = CalculateForThreshold1();
                    break;
                case ThresholdEnum.Threshold2:
                    calculatedAmount = CalculateForThreshold2();
                    break;
                case ThresholdEnum.Threshold3:
                    calculatedAmount = CalculateForThreshold3();
                    break;
                case ThresholdEnum.Threshold4:
                    calculatedAmount = CalculateForThreshold4();
                    break;
                case ThresholdEnum.Threshold5:
                    calculatedAmount = CalculateForThreshold5();
                    break;
            }

            return calculatedAmount;
        }

        protected override ThresholdEnum SelectThreshold()
        {
            ThresholdEnum threshold = ThresholdEnum.Undefined;
            
            if (Amount <= 8000m)
            {
                threshold = ThresholdEnum.Threshold1;
            } 
            else if (Amount >= 8001m && Amount <= 13000m)
            {
                threshold = ThresholdEnum.Threshold2;
            }
            else if (Amount >= 13001m && Amount <= 85528m)
            {
                threshold = ThresholdEnum.Threshold3;
            }
            else if (Amount >= 85529m && Amount <= 127000m)
            {
                threshold = ThresholdEnum.Threshold4;
            }
            else if (Amount >= 127001m)
            {
                threshold = ThresholdEnum.Threshold5;
            }

            return threshold;
        }

        // calculate decreasing tax for amount equal or lower then 8000 zl
        // calculation: allways returns 1420 zł
        private decimal CalculateForThreshold1()
        {
            return 1420m;
        }

        // calculate amount decreasing tax for amount between 8001 zl and 13000 zl
        // calculation: 1420 zł - [871 zł 70 gr × (Amount – 8000 zł) ÷ 5000 zł]
        private decimal CalculateForThreshold2()
        {
            decimal part1 = Amount - 8000m;
            decimal part2 = 871.70m * part1 / 5000m;

            return 1420m - part2;
        }

        // calculate amount decreasing tax for amount between 13001 zl and 85528 zl
        // calculation: allways returns 548.30 zł
        private decimal CalculateForThreshold3()
        {
            return 548.30m;
        }

        // calculate amount decreasing tax for amount between 85529 zl and 127000 zl
        // calculation: 548 zł 30 gr - [ 548 zł 30 gr × (Amount – 85 528 zł) ÷ 41 472 zł ]
        private decimal CalculateForThreshold4()
        {
            decimal part1 = Amount - 85528m;
            decimal part2 = 548.30m * part1 / 41472m;

            return 548.30m - part2;
        }

        // calculate amount decreasing tax for amount equal or greater then 127001 zl
        // calculation:  allways returns 0 zł
        private decimal CalculateForThreshold5()
        {
            return 0m;
        }
    }
}
