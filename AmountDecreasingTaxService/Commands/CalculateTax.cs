﻿namespace AmountDecreasingTaxService.Commands
{
    public abstract class CalculateTax
    {
        // input value
        public decimal Amount { get; set; }
        public CalculateAmountDecreasingTax DecreasingTaxCommand { get; set; }


        // output value
        public decimal? Result { get; protected set; }

        protected abstract decimal Calculate();

        public bool Invoke()
        {
            DecreasingTaxCommand.Amount = Amount;
            if (!DecreasingTaxCommand.Invoke())
                return false;

            decimal tax = Calculate();
            decimal amountDecreasingTax = DecreasingTaxCommand.Result.Value;


            Result = tax < amountDecreasingTax ? 0m : decimal.Round(tax - amountDecreasingTax);

            return true;
        }
    }
}




