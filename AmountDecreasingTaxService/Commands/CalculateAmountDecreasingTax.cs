﻿namespace AmountDecreasingTaxService.Commands
{
    public enum ThresholdEnum
    {
        Undefined,
        Threshold1,
        Threshold2,
        Threshold3,
        Threshold4,
        Threshold5
    }

    public abstract class CalculateAmountDecreasingTax
    {

        // input value
        public decimal Amount { get; set; }

        // output value
        public decimal? Result { get; protected set; }

        protected ThresholdEnum Threshold { get; set; }

        protected abstract ThresholdEnum SelectThreshold();
        protected abstract decimal Calculate();

        public bool Invoke()
        {
            Threshold = SelectThreshold();
            Result = Calculate();
            return true;
        }
    }
}


