﻿namespace AmountDecreasingTaxService.Commands
{
    public class CalculateTax2019 : CalculateTax
    {
        protected override decimal Calculate()
        {
            decimal calculatedAmount;
            if (Amount <= 85528m)
            {
                calculatedAmount = CalculateForThreshold1();
            }
            else
            {
                calculatedAmount = CalculateForThreshold2();
            }

            return calculatedAmount;
        }

        // For Amount equal or lower than 85.528 zł
        private decimal CalculateForThreshold1()
        {
            return 17.75m / 100m * Amount;
        }

        // For Amount greater than 85.528 zł
        private decimal CalculateForThreshold2()
        {
            decimal part1 = Amount - 85528m;
            decimal part2 = 32m / 100m * part1;

            return 15181.22m + part2;
        }
    }
}
