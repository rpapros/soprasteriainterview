using AmountDecreasingTaxService.Commands;
using System.Collections;
using System.Collections.Generic;
using Xunit;

namespace AmountDecreasingTaxService.Tests
{
    public class TestDataForDecreasingTax : IEnumerable<object[]>
    {
        private readonly List<object[]> _data = new List<object[]>
        {
            new object[] { 0, true, 1420m },
            new object[] { 4000, true, 1420m },
            new object[] { 8000, true, 1420m },
            new object[] { 8001, true, 1419.82566m },
            new object[] { 9000, true, 1245.66m },
            new object[] { 13000, true, 548.3m }
        };

        public IEnumerator<object[]> GetEnumerator() => _data.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }

    public class CalcAmountDecreasingTaxForThresholdsTest
    {
        [Theory]
        [ClassData(typeof(TestDataForDecreasingTax))]
        public void CalcAmountDecreasingTax2019Test(decimal amount, bool resultExecute, decimal? resultValue)
        {
            // Arrange 

            CalculateAmountDecreasingTax2019 alculateAmountDecreasingTax2019 = new CalculateAmountDecreasingTax2019
            {
                Amount = amount
            };

            // Act

            bool _resultExecute = alculateAmountDecreasingTax2019.Invoke();

            // Assert

            Assert.Equal(_resultExecute, resultExecute);
            Assert.Equal(alculateAmountDecreasingTax2019.Result, resultValue);
        }
    }
}
