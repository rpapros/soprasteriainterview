﻿using AmountDecreasingTaxService.Commands;
using System.Collections;
using System.Collections.Generic;
using Xunit;

namespace AmountDecreasingTaxService.Tests
{
    public class TestDataForCalcTax : IEnumerable<object[]>
    {
        private readonly List<object[]> _data = new List<object[]>
        {
            new object[] { 0, 0m },
            new object[] { 4000, 0m },
            new object[] { 8000, 0m },
            new object[] { 9000, 352m },
            new object[] { 13000, 1759m },
            new object[] { 65000, 10989m },
            new object[] { 85528, 14633m },
            new object[] { 85529, 14633m },
            new object[] { 100000, 19455m },
            new object[] { 127000, 28452m },
            new object[] { 127001, 28453m },
            new object[] { 150000, 35812m }
        };

        public IEnumerator<object[]> GetEnumerator() => _data.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }

    public class CalculateTaxTests
    {
        [Theory]
        [ClassData(typeof(TestDataForCalcTax))]
        public void CalculateTax2019Test(decimal amount, decimal? resultValue)
        {
            // Arrange 

            CalculateAmountDecreasingTax2019 alculateAmountDecreasingTax2019 = new CalculateAmountDecreasingTax2019
            {
                Amount = amount
            };

            CalculateTax2019 calculateTask2019 = new CalculateTax2019()
            {
                Amount = amount,
                DecreasingTaxCommand = alculateAmountDecreasingTax2019
            };

            // Act

            calculateTask2019.Invoke();

            // Assert

            Assert.Equal(calculateTask2019.Result, resultValue);
        }
    }
}
